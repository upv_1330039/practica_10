empleadoServicio = (function(){

      var encuentraPorId = function(id){
         var deferred = $.Deferred();
         var empleado = null;
         var l = empleados.length;
         for (var i=0; i<l; i++){
            if(empleados[i].id == id) {
               empleado = empleados[i];
               break;
            }
          }
         deferred.resolve(empleado);
         return deferred.promise();
   },

encuentraPorNombre = function(buscaTecla){
    var deferred = $.Deferred();
    var resultados = empleados.filter(function(elemento) {
         var nombreCompleto = elemento.nombre + " "+ elemento.apellido;
return nombreCompleto.toLowerCase().indexOf(buscaTecla.toLowerCase()) > -1;
});
deferred.resolve(resultados);
return deferred.promise();
},

encuentraPorManager = function (managerId) {
            var deferred = $.Deferred();
            var resultados = empleados.filter(function (elemento) {
                return managerId === elemento.managerId;
            });
            deferred.resolve(resultados);
            return deferred.promise();
        },

        empleados = [
            {"id": 1, "nombre": "Jaime", "apellido": "Reyes", "managerId": 0, "nombreManager": "", "reportaA": 4, "cargo": "Presidente y CEO", "departmento": "Corporativo", "telefonoMovil": "833-000-0001", "telefonoOficina": "834-000-0001", "email": "jking@upv.edu.mx", "ciudad": "Victoria, TAMPS", "foto": "james_king.jpg", "twitterId": "@fakejking", "blog": "http://upv.edu.mx"},
            {"id": 2, "nombre": "Julieta", "apellido": "Taylor", "managerId": 1, "nombreManager": "Jaime Reyes", "reportaA": 2, "cargo": "VP de Mercadotecnia", "departmento": "Mercadotecnia", "telefonoMovil": "833-000-0002", "telefonoOficina": "834-000-0002", "email": "jtaylor@upv.edu.mx", "ciudad": "Victoria, TAMPS", "foto": "julie_taylor.jpg", "twitterId": "@fakejtaylor", "blog": "http://upv.edu.mx"},
            {"id": 3, "nombre": "Eugenio", "apellido": "Lee", "managerId": 1, "nombreManager": "Jaime Reyes", "reportaA": 0, "cargo": "CFO", "departmento": "Contabilidad", "telefonoMovil": "833-000-0003", "telefonoOficina": "834-000-0003", "email": "elee@upv.edu.mx", "ciudad": "Victoria, TAMPS", "foto": "eugene_lee.jpg", "twitterId": "@fakeelee", "blog": "http://upv.edu.mx"},
            {"id": 4, "nombre": "Juan", "apellido": "Williams", "managerId": 1, "nombreManager": "Jaime Reyes", "reportaA": 3, "cargo": "VP de Ingenieria", "departmento": "Ingenier�a", "telefonoMovil": "833-000-0004", "telefonoOficina": "834-000-0004", "email": "jwilliams@upv.edu.mx", "ciudad": "Victoria, TAMPS", "foto": "john_williams.jpg", "twitterId": "@fakejwilliams", "blog": "http://upv.edu.mx"},
            {"id": 5, "nombre": "Raymundo", "apellido": "Moore", "managerId": 1, "nombreManager": "Jaime Reyes", "reportaA": 2, "cargo": "VP de Ventas", "departmento": "Ventas", "telefonoMovil": "833-000-0005", "telefonoOficina": "834-000-0005", "email": "rmoore@upv.edu.mx", "ciudad": "Victoria, TAMPS", "foto": "ray_moore.jpg", "twitterId": "@fakermoore", "blog": "http://upv.edu.mx"},
            {"id": 6, "nombre": "Pablo", "apellido": "Jones", "managerId": 4, "nombreManager": "Juan Williams", "reportaA": 0, "cargo": "Manager de QA", "departmento": "Ingenier�a", "telefonoMovil": "833-000-0006", "telefonoOficina": "834-000-0006", "email": "pjones@upv.edu.mx", "ciudad": "Victoria, TAMPS", "foto": "paul_jones.jpg", "twitterId": "@fakepjones", "blog": "http://upv.edu.mx"},
            {"id": 7, "nombre": "Paula", "apellido": "Gates", "managerId": 4, "nombreManager": "Juan Williams", "reportaA": 0, "cargo": "Arquitecto de Software", "departmento": "Ingenier�a", "telefonoMovil": "833-000-0007", "telefonoOficina": "834-000-0007", "email": "pgates@upv.edu.mx", "ciudad": "Victoria, TAMPS", "foto": "paula_gates.jpg", "twitterId": "@fakepgates", "blog": "http://upv.edu.mx"},
            {"id": 8, "nombre": "Lisa", "apellido": "Wong", "managerId": 2, "nombreManager": "Julieta Taylor", "reportaA": 0, "cargo": "Manager de Mercadotecnia", "departmento": "Mercadotecnia", "telefonoMovil": "833-000-0008", "telefonoOficina": "834-000-0008", "email": "lwong@upv.edu.mx", "ciudad": "Victoria, TAMPS", "foto": "lisa_wong.jpg", "twitterId": "@fakelwong", "blog": "http://upv.edu.mx"},
            {"id": 9, "nombre": "Gary", "apellido": "Donovan", "managerId": 2, "nombreManager": "Julieta Taylor", "reportaA": 0, "cargo": "Manager de Mercadotecnia", "departmento": "Mercadotecnia", "telefonoMovil": "833-000-0009", "telefonoOficina": "834-000-0009", "email": "gdonovan@upv.edu.mx", "ciudad": "Victoria, TAMPS", "foto": "gary_donovan.jpg", "twitterId": "@fakegdonovan", "blog": "http://upv.edu.mx"},
            {"id": 10, "nombre": "Catalina", "apellido": "Creel", "managerId": 5, "nombreManager": "Rauymundo Moore", "reportaA": 0, "cargo": "Representante de Ventas", "departmento": "Ventas", "telefonoMovil": "833-000-0010", "telefonoOficina": "834-000-0010", "email": "kbyrne@upv.edu.mx", "ciudad": "Victoria, TAMPS", "foto": "kathleen_byrne.jpg", "twitterId": "@fakekbyrne", "blog": "http://upv.edu.mx"},
            {"id": 11, "nombre": "Amanda", "apellido": "Jones", "managerId": 5, "nombreManager": "Raymundo Moore", "reportaA": 0, "cargo": "Representante de Ventas", "departmento": "Ventas", "telefonoMovil": "833-000-0011", "telefonoOficina": "834-000-0011", "email": "ajones@upv.edu.mx", "ciudad": "Victoria, TAMPS", "foto": "amy_jones.jpg", "twitterId": "@fakeajones", "blog": "http://upv.edu.mx"},
            {"id": 12, "nombre": "Esteban", "apellido": "Bodoque", "managerId": 4, "nombreManager": "Juan Williams", "reportaA": 0, "cargo": "Arquitecto de Software", "departmento": "Ingenieria", "telefonoMovil": "833-000-0012", "telefonoOficina": "834-000-0012", "email": "swells@upv.edu.mx", "ciudad": "Victoria, TAMPS", "foto": "steven_wells.jpg", "twitterId": "@fakeswells", "blog": "http://upv.edu.mx"},
            {"id": 13, "nombre": "Yanin", "apellido": "Yuen", "managerId": 3, "nombreManager": "Juan Williams", "reportaA": 0, "cargo": "Arquitecto de Software", "departmento": "Ingenieria", "telefonoMovil": "834-309-05-36", "telefonoOficina": "834-31-2-95-85", "email": "1330039@upv.edu.mx", "ciudad": "Victoria, TAMPS", "foto": "valeria.jpg", "twitterId": "@valeria_yuen", "blog": "http://upv.edu.mx"}
        ];

    // API p�blico
    return {
        encuentraPorId: encuentraPorId,
        encuentraPorNombre: encuentraPorNombre,
        encuentraPorManager: encuentraPorManager
    };

}());










